PDF_NAME = build/projectseminar-final-pres

all: build_pdf

build_pdf:
	latexmk -pdf -lualatex

optimize: build
	# compress
	gs -sDEVICE=pdfwrite -dUseCropBox -dPDFSETTINGS=/printer -dNOPAUSE -dBATCH -dDetectDuplicateImages=true -sOutputFile=${PDF_NAME}-comp.pdf ${PDF_NAME}.pdf
	# fast web view
	qpdf --linearize ${PDF_NAME}-comp.pdf ${PDF_NAME}-opt.pdf

spellcheck:
	find . -name "*.tex" -exec hunspell -d en_US -t -i utf-8 '{}' \;

clean:
	latexmk -CA
